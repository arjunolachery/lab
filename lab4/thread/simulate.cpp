#include <bits/stdc++.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

using namespace std;

long hare = 0;
long turtle = 0;
long hare_time = 0;
long turtle_time = 0;
int count_god=0;

const long TARGET = 50;
const long steps = 5;
const long dist_diff_sleep = 30;

pthread_mutex_t turtle_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t hare_mutex     = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t terminal_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t god_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t control_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t control_mutex2 = PTHREAD_MUTEX_INITIALIZER;


// thread for turtle
void *turtle_turn(void *args) {
 cout <<endl<<"----turtle1----"<<endl;
    while(turtle < TARGET) {
         cout <<endl<<"----turtle2----"<<endl;
        pthread_mutex_lock (&terminal_mutex);
        pthread_mutex_lock (&turtle_mutex);
        pthread_mutex_lock (&hare_mutex);
        cout <<endl<<"----turtle2in----"<<endl;
        turtle++;
        turtle_time++;
        pthread_mutex_unlock (&terminal_mutex);
        pthread_mutex_unlock (&turtle_mutex);
        pthread_mutex_unlock (&hare_mutex);
        pthread_mutex_lock (&control_mutex);
        cout <<endl<<"----turtle2out----"<<endl;
    }
    return (void *) 0;

}

// thread for hare
void *hare_turn(void *args) {
     cout <<endl<<"----hare----"<<endl;
    while(hare < TARGET) {
        cout <<endl<<"----hare2----"<<endl;
        if(hare-turtle >= dist_diff_sleep) {
            // hare sleeps for a random amount of time
            long sleep_time = rand()%(100000);
            hare_time += sleep_time;

            long random_sleep = rand()%1000; // between 0 and 999
            // make hare sleep for a random time
            usleep(random_sleep);
        }
        pthread_mutex_lock (&terminal_mutex);
        pthread_mutex_lock (&turtle_mutex);
        pthread_mutex_lock (&hare_mutex);
        cout <<endl<<"----hare2in----"<<endl;
        hare += steps;
        hare_time++;
        pthread_mutex_unlock (&terminal_mutex);
        pthread_mutex_unlock (&turtle_mutex);
        pthread_mutex_unlock (&hare_mutex);
        pthread_mutex_lock (&control_mutex);
        cout <<endl<<"----hare2out----"<<endl;
    }
    return (void *) 0;

}

// thread for reporter
void *reporter_turn(void *args) {
    long prev_hare=0;
    pthread_mutex_lock (&control_mutex);
    pthread_mutex_lock (&control_mutex2);
 cout <<endl<<"----reporter----"<<endl;
    while(turtle < TARGET || hare < TARGET) {
        cout <<endl<<"----reporter2----"<<endl;
        pthread_mutex_lock (&terminal_mutex);
        pthread_mutex_lock (&turtle_mutex);
        pthread_mutex_lock (&hare_mutex);
        cout <<endl<<"----reporter2in----"<<endl;


        cout << endl;
        for (int i = 0; i < 52; i++)
            cout << "=";
        cout << "|" << endl;
        cout << ">>";
        for (int i = 0; i < 50; i++) {
            if (hare == i)
                cout << "H";
            else
                cout << "-";
        }
        cout << "| Hare: " << hare << "m ";
        if(prev_hare != hare)
            cout <<"(5m/s)"<<endl;
        else
            cout <<"(Sleeping)"<<endl;
        cout << ">>";
        for (int i = 0; i < 50; i++) {
            if (turtle == i)
                cout << "T";
            else
                cout << "-";
        }
        cout << "| Turtle: " << turtle << "m (1m/s)" << endl;
        cout << "= 1m =";
        for (int i = 0; i < 18; i++)
            cout << "=";
        cout << " 25m ";
        for (int i = 0; i < 18; i++)
            cout << "=";
        cout << " 50m |" << endl
             << endl;
        
        prev_hare=hare;

        count_god=1;

        pthread_mutex_unlock (&terminal_mutex);
        pthread_mutex_unlock (&turtle_mutex);
        pthread_mutex_unlock (&hare_mutex);
        pthread_mutex_unlock (&control_mutex);
        pthread_mutex_unlock (&control_mutex2);
        cout <<endl<<"----reporter2out---"<<endl;
        if(turtle==50)
        {
        cout<<"\n\n===========================  Turtle won the race  ===========================\n\n";
        exit(1);
        }
        if(hare==50)
        {
        cout<<"\n\n===========================  Hare won the race  ===========================\n\n";
        exit(1);
        }
        usleep(500);
    }
    return (void *) 0;

}

void *god_turn(void *args) {
    int godval;
    char choice;
    while(turtle < TARGET && hare < TARGET) {
        pthread_mutex_lock (&control_mutex2);
        pthread_mutex_lock (&terminal_mutex);
        pthread_mutex_lock (&turtle_mutex);
        pthread_mutex_lock (&hare_mutex);
        if(count_god==1)
        {
        cout << "God, change positions of hare/turtle? (y/n)\n >> ";
        cin >> choice;
        if (choice == 'y') {
    cout << "Enter turtle's new position : ";
    cin >> turtle;
    cout << "Enter hare's new position : ";
    cin >> hare;
    
        }
        count_god=0;
        }
            pthread_mutex_unlock (&terminal_mutex);
            pthread_mutex_unlock (&turtle_mutex);
            pthread_mutex_unlock (&hare_mutex);
            pthread_mutex_unlock (&control_mutex2);
            // cout <<endl<<"----god2out---"<<endl;
    
    }
    return (void *) 0;
}

// main function
int main() {

    srand (time(NULL));

    pthread_t turtle_tid, hare_tid, reporter_tid, god_tid;

    pthread_create (&god_tid, NULL, god_turn, NULL);
    pthread_create (&turtle_tid, NULL, turtle_turn, NULL);
    pthread_create (&hare_tid, NULL, hare_turn, NULL);
    pthread_create (&reporter_tid, NULL, reporter_turn, NULL);

     pthread_join (god_tid, NULL);
    pthread_join (turtle_tid, NULL);
    pthread_join (hare_tid, NULL);
    pthread_join (reporter_tid, NULL);
   
}