#include <bits/stdc++.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

struct msg_buffer {
    long msg_type;
    int msg;
} message;

int main()
{
    key_t my_key;
    int msg_id;
    my_key = ftok("progfile", 65);
    msg_id = msgget(my_key, 0666 | IPC_CREAT);

    int hare = 0;
    int turtle = 0;
    const int steps = 5;
    const int dist_diff_sleep = 30;
    srand((unsigned int)time(NULL));
    int sleep_duration = rand() % 100;
    int sleep_counter = -1;

    cout << "Hare's randomly generated sleep duration: " << sleep_duration <<"s\n----------"<<endl<<endl;

    while (true) {
    
        if (sleep_counter != -1) {
            sleep_counter++;
            msgrcv(msg_id, &message, sizeof(message), 2, 0);
            hare = message.msg;

            msgrcv(msg_id, &message, sizeof(message), 3, 0);
            turtle = message.msg;

            message.msg_type = 5;
            message.msg = hare;
            msgsnd(msg_id, &message, sizeof(message), 0);

            if (sleep_counter >= sleep_duration) {
                sleep_counter = -1;
            }
            continue;
        }
        msgrcv(msg_id, &message, sizeof(message), 2, 0);
        hare = message.msg;

        msgrcv(msg_id, &message, sizeof(message), 3, 0);
        turtle = message.msg;
        hare += steps;
        message.msg_type = 5;
        message.msg = hare;
        msgsnd(msg_id, &message, sizeof(message), 0);

        if (hare - turtle >= dist_diff_sleep) {
            sleep_counter = 0;
        }
    }
    return 0;
}