#include <bits/stdc++.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

struct msg_buffer {
    long msg_type;
    int msg;
} message;

int main()
{
    key_t my_key;
    int msg_id;
    my_key = ftok("progfile", 65);
    msg_id = msgget(my_key, 0666 | IPC_CREAT);

    int turtle, hare;
    cout << "Enter turtle's new position : ";
    cin >> turtle;
    cout << "Enter hare's new position : ";
    cin >> hare;

    message.msg_type = 6;
    message.msg = turtle;
    msgsnd(msg_id, &message, sizeof(message), 0);

    message.msg_type = 6;
    message.msg = hare;
    msgsnd(msg_id, &message, sizeof(message), 0);

    return 0;
}