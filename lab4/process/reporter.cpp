#include <bits/stdc++.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

struct msg_buffer {
    long msg_type;
    int msg;
} message;

int main()
{

    key_t my_key;
    int msg_id;
    my_key = ftok("progfile", 65);
    msg_id = msgget(my_key, 0666 | IPC_CREAT);
    int share_msg = 0;
    int turtle, hare,prev_hare=0;
    while (true) {
        msgrcv(msg_id, &message, sizeof(message), 7, 0);
        turtle = message.msg;
        msgrcv(msg_id, &message, sizeof(message), 7, 0);
        hare = message.msg;
        cout << endl;
        for (int i = 0; i < 52; i++)
            cout << "=";
        cout << "|" << endl;
        cout << ">>";
        for (int i = 0; i < 50; i++) {
            if (hare == i)
                cout << "H";
            else
                cout << "-";
        }
        cout << "| Hare: " << hare << "m ";
        if(prev_hare != hare)
            cout <<"(5m/s)"<<endl;
        else
            cout <<"(Sleeping)"<<endl;
        cout << ">>";
        for (int i = 0; i < 50; i++) {
            if (turtle == i)
                cout << "T";
            else
                cout << "-";
        }
        cout << "| Turtle: " << turtle << "m (1m/s)" << endl;
        cout << "= 1m =";
        for (int i = 0; i < 18; i++)
            cout << "=";
        cout << " 25m ";
        for (int i = 0; i < 18; i++)
            cout << "=";
        cout << " 50m |" << endl
             << endl;

        if (turtle >= 50 || hare >= 50) {
            break;
        }
        prev_hare=hare;
    }

    return 0;
}