// Run make to execute hare and turtle simulation
#include <bits/stdc++.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>

using namespace std;

char* reporter_args[] = { "./reporter.out", NULL };
char* god_args[] = { "./god.out", NULL };
char* turtle_args[] = { "./turtle.out", NULL };
char* hare_args[] = { "./hare.out", NULL };

struct msg_buffer {
    long msg_type;
    int msg;
} message;

int main()
{

    key_t my_key;
    int msg_id;
    my_key = ftok("progfile", 65);
    msg_id = msgget(my_key, 0666 | IPC_CREAT);

    int turtle = 0;
    int hare = 0;
    const int TARGET = 50;
    int pid_turtle, pid_hare, pid_reporter, pid_god;

    if ((pid_turtle = fork()) == 0) {
        execv(turtle_args[0], turtle_args);
    }

    message.msg_type = 1;

    message.msg = turtle;
    msgsnd(msg_id, &message, sizeof(message), 0);

    if ((pid_hare = fork()) == 0) {
        execv(hare_args[0], hare_args);
    }

    message.msg_type = 2;

    message.msg = hare;
    msgsnd(msg_id, &message, sizeof(message), 0);

    if ((pid_reporter = fork()) == 0) {
        execv(reporter_args[0], reporter_args);
    }

    int winner = 0;
    int share_msg = 0;
    while (1) {

        msgrcv(msg_id, &message, sizeof(message), 4, 0);
        turtle = message.msg;

        msgrcv(msg_id, &message, sizeof(message), 5, 0);
        hare = message.msg;

        char choice;
        usleep(500);
        cout << "God, change positions of hare/turtle? (y/n)\n >> ";
        cin >> choice;
        if (choice == 'y') {
            if ((pid_god = fork()) == 0) {
                execv(god_args[0], god_args);
            }
            msgrcv(msg_id, &message, sizeof(message), 6, 0);
            turtle = message.msg;

            msgrcv(msg_id, &message, sizeof(message), 6, 0);
            hare = message.msg;
            wait(NULL);
        }

        if (hare >= TARGET) {
            winner = 1;
            break;
        }
        else if (turtle >= TARGET) {
            break;
        }

        message.msg_type = 7;

        message.msg = turtle;
        msgsnd(msg_id, &message, sizeof(message), 0);

        message.msg_type = 7;

        message.msg = hare;
        msgsnd(msg_id, &message, sizeof(message), 0);

        message.msg_type = 1;

        message.msg = turtle;
        msgsnd(msg_id, &message, sizeof(message), 0);

        message.msg_type = 2;

        message.msg = hare;
        msgsnd(msg_id, &message, sizeof(message), 0);
    }
    printf("\n%s won the race!", (winner == 0) ? "Turtle" : "Hare");

    kill(pid_turtle, SIGTERM);
    kill(pid_reporter, SIGTERM);
    kill(pid_hare, SIGTERM);

    msgctl(msg_id, IPC_RMID, NULL);
    return 0;
}
